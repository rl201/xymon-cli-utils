# Xymon Cli Utils

Some (fairly shoddy) shell scripts to interact with Xymon

# bb-color colour [test]

bb-color red - show a summary of all the tests which are red
bb-color red libs - show list of all the machines with red libs dot

# bb-do test colour match action

bb-do cpu red dropbox "killall dropbox" - kill all dropbox processes on systems with a high cpu load; runs in series

# bb-grep test [colour] [pattern]

bb-grep cpu - shows all machines reporting a CPU dot
bb-grep cpu red - shows all machines reporting a red CPU dot
bb-grep cpu red dropbox - shows all machines reporting a red CPU dot which contains the string dropbox

# bb-pdo test colour match action

bb-pdo cpu red dropbox "killall dropbox" - kill all dropbox processes on systems with a high cpu load; runs in parallel, up to 200 systems at a time.

# bb-pie 

bb-pie - shows percentage of test in each colour

# bb-test host test

bb-test cludge cpu - shows the contents of the CPU test on cludge.damtp.cam.ac.uk

# bb-tests host

bb-tests cludge - shows the tests reported by cludge.damtp.cam.ac.uk and their current colour.

# bb-wc colour test

bb-wc red libs - reports the number of lines in all the red libs tests, per host.
